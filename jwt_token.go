package express

import (
	"errors"
	"time"

	"github.com/dgrijalva/jwt-go"
)

const ()

// UserClaims jwt the user claims
type UserClaims struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
	jwt.StandardClaims
}

// Valid checks whether the token is valid or not
func (uc *UserClaims) Valid() error {
	now := time.Now().Unix()
	if !uc.VerifyAudience(envJwtAud, true) {
		return errors.New("Missing audience")
	}

	if !uc.VerifyExpiresAt(now, true) {
		return errors.New("Token has expired")
	}

	if !uc.VerifyIssuedAt(now, true) {
		return errors.New("Token is used before issued")
	}
	return nil
}
