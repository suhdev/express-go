package express

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
)

// Encrypter content encrypter
type Encrypter func(content []byte) ([]byte, error)

// Decrypter content decrypter
type Decrypter func(content []byte) ([]byte, error)

// CreateEncrypter creates an encrypter function given a public key
func CreateEncrypter(publicKey *rsa.PublicKey) Encrypter {
	return func(content []byte) ([]byte, error) {
		return rsa.EncryptOAEP(sha256.New(), rand.Reader, publicKey, content, []byte("s3cr3t"))
	}
}

// CreateDecrypter creates a decrypter function given a private key
func CreateDecrypter(privateKey *rsa.PrivateKey) Decrypter {
	return func(content []byte) ([]byte, error) {
		return rsa.DecryptOAEP(sha256.New(), rand.Reader, privateKey, content, []byte("s3cr3t"))
	}
}
