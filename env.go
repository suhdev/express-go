package express

import (
	"crypto"
	"log"
	"os"
)

const (
	// EnvJwtPrivateKey the jwt private key
	EnvJwtPrivateKey = "JWT_PRIVATE_KEY"
	// EnvJwtAlg the jwt algorithm
	EnvJwtAlg = "JWT_ALG"
	// EnvJwtAud the jwt audience
	EnvJwtAud = "JWT_AUD"
	// EnvCookieDomain the cookie domain used for cookies
	EnvCookieDomain = "COOKIE_DOMAIN"
	// EnvEncPublicKey public key
	EnvEncPublicKey = "ENC_PUBLIC_KEY"
	// EnvEncPrivateKey private key
	EnvEncPrivateKey = "ENC_PRIVATE_KEY"
)

var (
	envJwtPrivateKey = ""
	envJwtAlg        = ""
	envJwtAud        = ""
	envCookieDomain  = ""
	envEncPrivateKey = ""
	envEncPublicKey  = ""
	privateKey       crypto.PrivateKey
	publicKey        crypto.PublicKey
)

func ensureEnvVar(key string) string {
	var v string
	var ok bool
	if v, ok = os.LookupEnv(key); !ok {
		log.Fatalf("Environment variable %s could not be found", EnvJwtPrivateKey)
	}
	return v
}

func init() {
	envJwtPrivateKey = ensureEnvVar(EnvJwtPrivateKey)
	envJwtAlg = ensureEnvVar(EnvJwtAlg)
	envJwtAud = ensureEnvVar(EnvJwtAud)
	envCookieDomain = ensureEnvVar(EnvCookieDomain)
	envEncPublicKey = ensureEnvVar(EnvEncPublicKey)
	envEncPrivateKey = ensureEnvVar(EnvEncPrivateKey)
}
