package express

// UUID unique user id
type UUID string

// User the user structure
type User struct {
	// ID the user identifier
	ID UUID
	// FirstName the user first name
	FirstName string
	// LastName the user last name
	LastName string
}
