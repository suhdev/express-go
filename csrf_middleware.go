package express

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"
)

// CsrfTokenMiddleware creates a CSRF token extraction middleware
// given encryption key
func CsrfTokenMiddleware(key, csrfFieldName string) Middleware {
	return func(data *RequestData, req *http.Request, resp http.ResponseWriter, next NextCallback) {
		if data.Body != nil && strings.ToLower(req.Method) == "post" {
			if req.Header.Get("Content-Type") == "application/json" ||
				req.Header.Get("Content-Type") == "application/x-www-form-urlencoded" {
				buf := data.Body.Bytes()
				mdata := make(map[string]interface{})
				err := json.Unmarshal(buf, &mdata)
				if err == nil {
					if val, found := mdata[csrfFieldName]; found {
						if v, ok := val.(int64); ok {
							data.CsrfTokenRawValue = v
							data.CsrfTokenParsedValue = time.Unix(v, 0)
						}
					}
				}
			}

		}
		next()
	}
}

// CsrfTokenGenerateMiddleware creates a CSRF token extraction middleware
// given encryption key
func CsrfTokenGenerateMiddleware(key, csrfFieldName string) Middleware {
	return func(data *RequestData, req *http.Request, resp http.ResponseWriter, next NextCallback) {
		data.CsrfTokenParsedValue = time.Now().Add(time.Minute * 10)
		data.CsrfTokenRawValue = data.CsrfTokenParsedValue.Unix()
		http.SetCookie(resp, &http.Cookie{
			HttpOnly: true,
			Domain:   envCookieDomain,
			Secure:   true,
			Expires:  data.CsrfTokenParsedValue,
		})
		next()
	}
}
