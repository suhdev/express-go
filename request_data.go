package express

import (
	"bytes"
	"time"
)

// RequestData data associated with the request
type RequestData struct {
	// CsrfTokenRawValue the raw csrf token associated with the request
	CsrfTokenRawValue int64
	// CsrfTokenParsedValue the parsed value of the csrf token associated with the request
	CsrfTokenParsedValue time.Time
	// CsrfGeneratedRawValue the generated value of the new csrf
	// token to be sent as part of the response
	CsrfGeneratedRawValue int64
	// CsrfGeneratedParsedValue the parsed value of the new csrf
	// token to be sent as part of the response
	CsrfGeneratedParsedValue time.Time
	// User the logged in user if any
	User *User
	// Body the request body buffer
	Body *bytes.Buffer
}
