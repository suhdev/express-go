package express

import (
	"bytes"
	"net/http"
)

// BodyReaderMiddleware reads the body of an http request if it has one
func BodyReaderMiddleware(data *RequestData, req *http.Request, resp http.ResponseWriter, next NextCallback) {
	buf := bytes.NewBuffer([]byte{})
	if req.Body == nil {
		next()
	}
	buf.ReadFrom(req.Body)
	data.Body = buf
	next()
}
