package express

import (
	"fmt"
	"net/http"

	"github.com/dgrijalva/jwt-go"
)

// JwtExtractMiddleware a middleware to extract jwt token from http cookies
func JwtExtractMiddleware(key string) Middleware {
	return func(data *RequestData, req *http.Request, resp http.ResponseWriter, next NextCallback) {
		cookie, err := req.Cookie("authorisation")
		if err != nil {
			next()
			return
		}

		u := &UserClaims{}
		_, err = jwt.ParseWithClaims(cookie.Value, u, func(t *jwt.Token) (interface{}, error) {
			if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Invalid authentication token")
			}

			return []byte(envJwtPrivateKey), nil
		})

		if err != nil {
			next()
			return
		}

		data.User = &User{
			FirstName: u.FirstName,
			LastName:  u.LastName,
			ID:        UUID(u.Id),
		}
		next()
	}
}
