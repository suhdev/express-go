package express

import (
	"net/http"
	"strings"
)

// NextCallback progress the pipeline
type NextCallback func()

// Middleware a middleware handler
type Middleware func(data *RequestData, req *http.Request, resp http.ResponseWriter, next NextCallback)

// Application a basic web application
type Application struct {
	middlewares []Middleware
}

// Use adds middlewares to the pipeline
func (a *Application) Use(m ...Middleware) {
	a.middlewares = append(a.middlewares, m...)
}

func createPipeline(handle ...Middleware) Middleware {
	return func(data *RequestData, req *http.Request, resp http.ResponseWriter, next NextCallback) {
		var n NextCallback
		var idx = 0
		n = func() {
			idx++
			if idx < len(handle) {
				handle[idx](data, req, resp, n)
				return
			}
			next()
		}

		handle[0](data, req, resp, n)
	}
}

func createRequestMiddleware(path, method string, handle ...Middleware) Middleware {
	pipeline := createPipeline(handle...)

	return func(data *RequestData, req *http.Request, resp http.ResponseWriter, next NextCallback) {
		if strings.ToLower(req.Method) != method || req.URL.Path != path {
			next()
			return
		}
		pipeline(data, req, resp, next)
	}
}

// Get adds a get-based middleware
func (a *Application) Get(path string, middlewares ...Middleware) {
	a.middlewares = append(a.middlewares, createRequestMiddleware(path, "get", middlewares...))
}

// Post adds a post-based middleware
func (a *Application) Post(path string, middlewares ...Middleware) {
	a.middlewares = append(a.middlewares, createRequestMiddleware(path, "post", middlewares...))
}

// Put adds a Put-based middleware
func (a *Application) Put(path string, middlewares ...Middleware) {
	a.middlewares = append(a.middlewares, createRequestMiddleware(path, "put", middlewares...))
}

// Delete adds a Delete-based middleware
func (a *Application) Delete(path string, middlewares ...Middleware) {
	a.middlewares = append(a.middlewares, createRequestMiddleware(path, "delete", middlewares...))
}

// Patch adds a Patch-based middleware
func (a *Application) Patch(path string, middlewares ...Middleware) {
	a.middlewares = append(a.middlewares, createRequestMiddleware(path, "patch", middlewares...))
}

// Handle handles requests
func (a *Application) Handle(resp http.ResponseWriter, req *http.Request) {
	pipeline := createPipeline(a.middlewares...)
	data := &RequestData{}
	pipeline(data, req, resp, func() {
	})
}

// NewApplication creates a new Application
func NewApplication() *Application {
	return &Application{
		middlewares: make([]Middleware, 0),
	}
}
