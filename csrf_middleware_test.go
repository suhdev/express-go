package express_test

import (
	"testing"

	"gitlab.com/suhdev/express-go"
)

func TestCsrfGenerateMiddleware(t *testing.T) {
	m := express.CsrfTokenGenerateMiddleware("some key", "csrfToken")
	rd := &express.RequestData{}
	called := false
	next := func() {
		called = true
	}
	m(rd, nil, nil, next)

	if !called {
		t.Fatalf("Expected next to be called but it didn't")
	}

	if rd.CsrfTokenRawValue == 0 {
		t.Fatalf("Expected csrf raw value to be set")
	}

	if rd.CsrfTokenParsedValue.Unix() == 0 {
		t.Fatalf("Expected CsrfTokenParsedValue to be set")
	}
}
